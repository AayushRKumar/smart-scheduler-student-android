package com.example.smartscheduler_student.ui.Schoolhours;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.smartscheduler_student.R;

public class SetSchoolHours extends Fragment {

    private SetSchoolHoursModel SetSchoolHoursModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        SetSchoolHoursModel =
                ViewModelProviders.of(this).get(SetSchoolHoursModel.class);
        View root = inflater.inflate(R.layout.fragment_set_school_hours, container, false);
        final TextView textView = root.findViewById(R.id.text_slideshow);
       textView.setText("Coded: School Hours");
        return root;
    }
}