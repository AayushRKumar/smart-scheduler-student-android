package com.example.smartscheduler_student.ui.ExtraCurs;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ExtraCurModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ExtraCurModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is tools fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}