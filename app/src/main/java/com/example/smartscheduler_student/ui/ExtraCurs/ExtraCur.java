package com.example.smartscheduler_student.ui.ExtraCurs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.smartscheduler_student.R;

public class ExtraCur extends Fragment {

    private ExtraCurModel extraCurModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        extraCurModel =
                ViewModelProviders.of(this).get(ExtraCurModel.class);
        View root = inflater.inflate(R.layout.fragment_extracur, container, false);
        final TextView textView = root.findViewById(R.id.text_tools);
       textView.setText("Coded: ExtraCurs");
        return root;
    }
}