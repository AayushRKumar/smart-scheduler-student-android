package com.example.smartscheduler_student.ui.Schoolhours;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SetSchoolHoursModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SetSchoolHoursModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}