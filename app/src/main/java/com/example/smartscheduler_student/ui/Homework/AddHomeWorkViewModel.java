package com.example.smartscheduler_student.ui.Homework;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AddHomeWorkViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AddHomeWorkViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}