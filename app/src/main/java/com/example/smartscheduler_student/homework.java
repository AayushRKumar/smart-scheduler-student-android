package com.example.smartscheduler_student;

public class homework {
    String name;
    int minutes;
    int priority;
    String notes;

    public homework(String name, int minutes, int priority, String notes ) {
        this.name = name;
        this.minutes = minutes;
        this.priority = priority;
        this.notes = notes;
    }
}
