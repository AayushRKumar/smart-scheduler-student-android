package com.example.smartscheduler_student;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<homework> classesList = new ArrayList<>();
    public class homeworkAdapter extends ArrayAdapter<homework> {

        private Context mContext;
        private List<homework> moviesList = new ArrayList<>();

        public homeworkAdapter( Context context, ArrayList<homework> list) {
            super(context, 0 , list);
            mContext = context;
            moviesList = list;
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View listItem = convertView;
            if(listItem == null)
                listItem = LayoutInflater.from(mContext).inflate(R.layout.homework_row,parent,false);
           //changing classeslist[position] to movielists

            homework currentHW = moviesList.get(position);


            TextView name = (TextView) listItem.findViewById(R.id.nameL);
            name.setText(currentHW.name);
            TextView min = (TextView) listItem.findViewById(R.id.minL);
            String minStr = Integer.toString(currentHW.minutes);
            String minTxt = minStr + " minutes";

            min.setText(minTxt);
            TextView pri = (TextView) listItem.findViewById(R.id.priL);
            pri.setText(Integer.toString(currentHW.priority));

            return listItem;
        }
    }

        private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Intialized");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_addhw, R.id.nav_setschoolhours,
                R.id.nav_addextracur, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        //testing list view


       // mListView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    public void enter(View view) {



        final EditText addhwNameTF = findViewById(R.id.classesTFID);
        final EditText addhwMinutesTF = findViewById(R.id.minutesTFID);
        final EditText addhwPriorityTF = findViewById(R.id.priorityTFID);
     //   final ListView classesTable = findViewById(R.id.classesTable);

        final String name = addhwNameTF.getText().toString();
        final String timeText = addhwMinutesTF.getText().toString();
        final String priorityText = addhwPriorityTF.getText().toString();
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(addhwNameTF.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(addhwPriorityTF.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(addhwMinutesTF.getWindowToken(), 0);
           Boolean makeReturn = false;
        if (name.matches("")) {

            makeReturn = true;
           //Add border turning red
        }
        if (timeText.matches("")) {

            makeReturn = true;
            //red
        }
        if (priorityText.matches("")) {

            makeReturn = true;
            //red
        }

        System.out.println("hwPressed");
        if (makeReturn) {
            makeReturn = false;
           System.out.println("MAKERETURN");
              alert("Invalid", "Please fill all the boxes", "OK");

            return;
        }
        System.out.println("PASSED MAKE RETURn");
        if ((Integer.parseInt(priorityText) < 1)||(Integer.parseInt(priorityText) > 10)) {
             alert("Invalid", "Priority Needs to be 1-10", "OK");
             return;
        }
        addhwMinutesTF.setText("");
        addhwNameTF.setText("");
        addhwPriorityTF.setText("");
        final EditText edittext = new EditText(this);
        new AlertDialog.Builder(this)

.setView(edittext)
                .setTitle("Notes")
                .setMessage("Enter Notes about the homework here")

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton("Enter", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ListView listView = findViewById(R.id.table);
                    int minFHW = Integer.parseInt(timeText);
                   int priFHW = Integer.parseInt(priorityText);

                        classesList.add(new homework(name, minFHW, priFHW, edittext.getText().toString()));

                        // homework[] ar = moviesList.toArray();

                        homeworkAdapter adap = new homeworkAdapter(MainActivity.this,classesList);
                        listView.setAdapter(adap);
                   save();
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.

                .setIcon(android.R.drawable.ic_dialog_info)
                .show();

    }

      void save() {

      }
        void alert(String title, String message, String buttonText)  {
            new AlertDialog.Builder(this)
                    .setTitle(title)
                    .setMessage(message)

//                    // Specifying a listener allows you to take an action before dismissing the dialog.
//                    // The dialog is automatically dismissed when a dialog button is clicked.
//                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            // Continue with delete operation
//                        }
//                    })

                    // A null listener allows the button to dismiss the dialog and take no further action.
                    .setNegativeButton(buttonText, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
    }
    public void clearHW(View view) {
        final EditText addhwNameTF = findViewById(R.id.classesTFID);
        final EditText addhwMinutesTF = findViewById(R.id.minutesTFID);
        final EditText addhwPriorityTF = findViewById(R.id.priorityTFID);
           addhwMinutesTF.setText("");
           addhwNameTF.setText("");
            addhwPriorityTF.setText("");
    }
    public void addHWscreenTapped(View view) {
        final EditText addhwNameTF = findViewById(R.id.classesTFID);
        final EditText addhwMinutesTF = findViewById(R.id.minutesTFID);
        final EditText addhwPriorityTF = findViewById(R.id.priorityTFID);
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(addhwNameTF.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(addhwPriorityTF.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(addhwMinutesTF.getWindowToken(), 0);

    }
}
